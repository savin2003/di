package com.asavin.di.retrofit;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Retrofit;

public class PBServiceAdapter {

    private PBService service;

    @Inject
    @Named("loged")
    public PBServiceAdapter(Retrofit retrofit) {

        service = retrofit.create(PBService.class);
    }

    public PBService getPBService() {
        return service;
    }
}
