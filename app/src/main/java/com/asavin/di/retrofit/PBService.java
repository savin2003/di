package com.asavin.di.retrofit;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

public interface PBService {

    @GET("p24api/pubinfo?json&exchange&coursid=3")
    Observable<List<CurrencyInfo>> getCurrencies();
}