package com.asavin.di.di.component;


import com.asavin.di.di.module.MvpActivityModule;
import com.asavin.di.di.scope.ActivityScope;
import com.asavin.di.mvp.MvpActivity;

import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = {MvpActivityModule.class})
public interface MvpActivityComponent {

    MvpActivity inject(MvpActivity activity);
}
