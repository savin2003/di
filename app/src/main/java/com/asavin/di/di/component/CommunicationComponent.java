package com.asavin.di.di.component;

import com.dteam.rxmvp.di.module.AppModule;
import com.dteam.rxmvp.di.module.CommunicationModule;
import com.dteam.rxmvp.retrofit.PBService;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {CommunicationModule.class}, dependencies = AppModule.class)
public interface CommunicationComponent {

    PBService pBService();
}
