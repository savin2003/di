package com.asavin.di.di.module;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.asavin.di.di.component.CommunicationComponent;
import com.asavin.di.retrofit.PBService;
import com.asavin.di.retrofit.PBServiceAdapter;

import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

;
;

@Module
public class CommunicationModule {

    public static final String BASE_URL = "https://api.privatbank.ua";

    @Provides
    @Singleton
    PBService providePBService(Retrofit retrofit) {
        return new PBServiceAdapter(retrofit).getPBService();
    }

    @Provides
    @Singleton
    SharedPreferences providesSharedPreferences(Application app) {
        return PreferenceManager.getDefaultSharedPreferences(app);
    }

    @Provides
    @Singleton
    @Named("simple")
    Retrofit provideRetrofitWithCache(OkHttpClient httpClient) {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(httpClient)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    Cache provideOkHttpCache(Application application) {
        int cacheSize = 10 * 1024 * 1024; // 10 MiB
        return new Cache(application.getCacheDir(), cacheSize);
    }
    @Provides
    @Named("loged")
    @Singleton
    OkHttpClient someHttpClient() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
//                Logger.getLogger(CommunicationModule.class);
                LoggerFactory.getLogger(CommunicationComponent.class).debug("begin");
                Request original = chain.request();
                Request request = original.newBuilder().build();
                Response response = chain.proceed(request);
                LoggerFactory.getLogger(CommunicationComponent.class).debug("end");
                return response;
            }
        });

        OkHttpClient client = httpClient.build();

        return client;



    }

    @Provides
    @Singleton
    @Named("some")
    OkHttpClient provideOkHttpClient(Cache cache, Interceptor interceptor) {
        return new OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(interceptor)
                .build();
    }

    @Provides
    @Singleton
    Interceptor provideCacheInterceptor() {
        return chain -> {
            Request request = chain.request();

            CacheControl cacheControl = new CacheControl.Builder()
                    .maxStale(1, TimeUnit.MINUTES)
                    .build();

            request = request.newBuilder()
                    .cacheControl(cacheControl)
                    .build();


            return chain.proceed(request);
        };
    }

}
