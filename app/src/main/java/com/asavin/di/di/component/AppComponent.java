package com.asavin.di.di.component;

import com.asavin.di.di.module.AppModule;
import com.asavin.di.di.module.CommunicationModule;
import com.asavin.di.di.module.MvpActivityModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, CommunicationModule.class})
public interface AppComponent {

    MvpActivityComponent plus(MvpActivityModule module);
}
