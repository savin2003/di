package com.asavin.di.di.module;

import com.asavin.di.di.scope.ActivityScope;
import com.asavin.di.mess.MessCurrencyAdapter;
import com.asavin.di.mvp.MvpActivity;
import com.asavin.di.mvp.presenter.MvpPresenter;
import com.asavin.di.mvp.presenter.impl.MvpPresenterImpl;
import com.asavin.di.retrofit.PBService;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

@Module
public class MvpActivityModule {

    private MvpActivity activity;

    public MvpActivityModule(MvpActivity activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    MvpActivity provideMvpActivity() {
        return activity;
    }

    @Provides
    @ActivityScope
    MvpPresenter provideMvpPresenter(PBService pbService) {
        return new MvpPresenterImpl(activity, pbService);
    }

    @Provides
    @ActivityScope
    MessCurrencyAdapter provideMessCurrencyAdapter() {
        return new MessCurrencyAdapter(new ArrayList<>());
    }
}
