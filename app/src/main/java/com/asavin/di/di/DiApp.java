package com.asavin.di.di;

import android.app.Application;

import com.asavin.di.di.component.AppComponent;
import com.asavin.di.di.module.AppModule;

//import com.dteam.rxmvp.di.component.AppComponent;
//import com.dteam.rxmvp.di.component.DaggerAppComponent;
//import com.dteam.rxmvp.di.module.AppModule;

public class DiApp extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();

    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
