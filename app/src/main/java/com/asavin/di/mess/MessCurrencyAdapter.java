package com.asavin.di.mess;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.asavin.di.R;
import com.asavin.di.retrofit.CurrencyInfo;

import java.util.List;

public class MessCurrencyAdapter extends RecyclerView.Adapter<MessCurrencyAdapter.CurrenciesHolder> {

    private List<CurrencyInfo> currencies;

    public MessCurrencyAdapter(List<CurrencyInfo> currencies) {
        this.currencies = currencies;
    }

    @Override
    public CurrenciesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.currency_item, parent, false);
        return new CurrenciesHolder(view);
    }

    @Override
    public void onBindViewHolder(CurrenciesHolder holder, int position) {
        holder.ccy.setText(currencies.get(position).getCcy());
        holder.buy.setText(currencies.get(position).getBuy());
        holder.sell.setText(currencies.get(position).getSale());
    }

    @Override
    public int getItemCount() {
        return currencies.size();
    }

    public void addData(List<CurrencyInfo> currencyInfo) {
        currencies.clear();
        currencies.addAll(currencyInfo);
        notifyDataSetChanged();
    }

    static class CurrenciesHolder extends RecyclerView.ViewHolder {

        private TextView ccy;
        private TextView buy;
        private TextView sell;

        public CurrenciesHolder(View itemView) {
            super(itemView);
            ccy = (TextView) itemView.findViewById(R.id.ccy);
            buy = (TextView) itemView.findViewById(R.id.buy);
            sell = (TextView) itemView.findViewById(R.id.sell);
        }
    }
}
