package com.asavin.di.mess;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.asavin.di.R;

import java.util.ArrayList;

import rx.Subscription;

public class MessStructureActivity extends AppCompatActivity {

    private static final String TAG = MessStructureActivity.class.getName();

    private Subscription loadDataSubscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mess_strucutre);

        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.currencies_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        MessCurrencyAdapter adapter = new MessCurrencyAdapter(new ArrayList<>());
        recyclerView.setAdapter(adapter);

        SwipeRefreshLayout refreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_to_load_currencies);
        /*refreshLayout.setOnRefreshListener(() -> loadDataSubscription = new PBServiceAdapter().getPBService().getCurrencies()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<CurrencyInfo>>() {
                    @Override
                    public void onCompleted() {
                        refreshLayout.setRefreshing(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        refreshLayout.setRefreshing(false);
                        Snackbar.make(recyclerView, R.string.connection_error, Snackbar.LENGTH_SHORT);
                    }

                    @Override
                    public void onNext(List<CurrencyInfo> currencyInfos) {
                        adapter.addData(currencyInfos);
                    }
                }));*/
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (loadDataSubscription != null) {
            loadDataSubscription.unsubscribe();
        }
    }
}
