package com.asavin.di.mvp;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.asavin.di.R;
import com.asavin.di.di.DiApp;
import com.asavin.di.di.module.MvpActivityModule;
import com.asavin.di.mess.MessCurrencyAdapter;
import com.asavin.di.mvp.presenter.MvpPresenter;
import com.asavin.di.retrofit.CurrencyInfo;

import java.util.List;

import javax.inject.Inject;

public class MvpActivity extends BaseActivity implements MvpView {

    @Inject
    MvpPresenter presenter;

    @Inject
    MessCurrencyAdapter adapter;

    private SwipeRefreshLayout refreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter.onCreate();

        setContentView(R.layout.activity_mvp);

        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.mvp_currencies_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.mvp_swipe_to_load_currencies);
        refreshLayout.setOnRefreshListener(() -> presenter.loadData());
    }

    @Override
    protected void setupActivityComponent() {
        ((DiApp) getApplication()).getAppComponent().plus(new MvpActivityModule(this)).inject(this);
    }

    @Override
    public void hideLoadProgress() {
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void setData(List<CurrencyInfo> data) {
        adapter.addData(data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }
}
