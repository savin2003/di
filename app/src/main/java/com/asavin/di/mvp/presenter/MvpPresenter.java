package com.asavin.di.mvp.presenter;

public interface MvpPresenter {

    void loadData();

    void onCreate();

    void onDestroy();
}
