package com.asavin.di.mvp;


import com.asavin.di.retrofit.CurrencyInfo;

import java.util.List;

public interface MvpView {

    void hideLoadProgress();

    void setData(List<CurrencyInfo> data);
}
