package com.asavin.di.mvp.presenter.impl;


import com.asavin.di.mvp.MvpView;
import com.asavin.di.mvp.presenter.MvpPresenter;
import com.asavin.di.retrofit.CurrencyInfo;
import com.asavin.di.retrofit.PBService;

import java.util.List;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MvpPresenterImpl implements MvpPresenter {

    private MvpView view;
//    iew
    private Subscription loadDataSubscription;

    private PBService pbService;

    public MvpPresenterImpl(MvpView view, PBService pbService) {
        this.pbService = pbService;
        this.view = view;
    }

    @Override
    public void loadData() {
        loadDataSubscription = pbService.getCurrencies()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<CurrencyInfo>>() {
                    @Override
                    public void onCompleted() {
                        view.hideLoadProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.hideLoadProgress();
                    }

                    @Override
                    public void onNext(List<CurrencyInfo> currencyInfos) {
                        view.setData(currencyInfos);
                    }
                });
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onDestroy() {
        if (loadDataSubscription != null) {
            loadDataSubscription.unsubscribe();
        }
    }
}
